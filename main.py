import mlflow
import pandas as pd
from mlflow.models.signature import ModelSignature
from mlflow.types import Schema, ColSpec
from sklearn.metrics import precision_score, recall_score, f1_score

from sklearn.model_selection import train_test_split
from sklearn.datasets import load_wine
from catboost import CatBoostClassifier

mlflow.set_tracking_uri("http://127.0.0.1:5000")
mlflow.set_experiment('CatBoost')

params = {
    'iterations': 1000,
    'learning_rate': 0.01,
    'depth': 5,
    'eval_metric': 'TotalF1',
    'early_stopping_rounds': 32,
    'use_best_model': True,
    'random_seed': 42,
}

with mlflow.start_run(run_name='CatBoost/0.01') as run:
    mlflow.set_tags(tags={
        "auto_tracking": "false",
        "framework": "Catboost"
    })

    db = load_wine()
    X_train, X_test, y_train, y_test = train_test_split(db.data, db.target, random_state=42)

    # Create and train models.
    model = CatBoostClassifier(**params)
    model.fit(X_train, y_train, eval_set=(X_test, y_test))

    # Use the model to make predictions on the test dataset.
    predictions = model.predict(X_test)

    # Log metric
    precision = precision_score(y_test, predictions, average='weighted')
    recall = recall_score(y_test, predictions, average='weighted')
    f1 = f1_score(y_test, predictions, average='weighted')

    mlflow.log_metric('precision', precision)
    mlflow.log_metric('recall', recall)
    mlflow.log_metric('f1', f1)

    # signature
    data = pd.DataFrame(data=db['data'], columns=db['feature_names'])
    input_schema = Schema([ColSpec('float', hd) for hd in list(data)])
    output_schema = Schema([ColSpec("long", "target")])
    signature = ModelSignature(inputs=input_schema, outputs=output_schema)

    # log model
    mlflow.log_params(params=params)
    mlflow.catboost.log_model(
        model,
        artifact_path="model",
        signature=signature,
        registered_model_name='catboost'
    )
