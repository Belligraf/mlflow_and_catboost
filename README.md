# mlflow_and_catboost


## How to install project 

### 1. Clone repository

    git clone https://gitlab.com/Belligraf/mlflow_and_catboost.git

### 2. Install requirements

    pip install -r requirements.txt
    poetry install

### 3. Command for run MLFlow server

    mlflow server

### 4. Command to start server with your model

    mlflow models serve --no-conda -h 0.0.0.0 -p 8001 -m {your_model_path}
    
    example model path mlartifacts/0/6266f7b678f74479b42e40dd9404ceab/artifacts/model


## What's in the files

### main.py

In this file, the catboost model is trained, on a dataset with wine, 
all parameters are logged to see the result of the models, run the following commands
since catboost does not have autologin, all the logs had to be written manually
    python main.py
    
    mlflow server

after you start the server in the experiments tab you will see the created model, 
if you want to see more information about a run just click on the title of the run
![alt text](images/result_after_run_main.png)

### linear_regression.py

This file exists to show exactly what mlflow is logging if you just enable autologging 
you can open the catboost and linear regression tab to compare the differences in logging

Run command

    python linear_regression.py

### client.ipynb
In this file, the model is uploaded to the server from the beginning, you need 
to change the state of the model from None to Stage, then run the following command

    mlflow models serve --no-conda -h 0.0.0.0 -p 8001 -m {your_model_path}
    
    example model path mlartifacts/0/6266f7b678f74479b42e40dd9404ceab/artifacts/model

after that, you can run cell 6 of the code and then test data will be sent to the server 
with the model and the predicted category will come in the response, after which 
I use the f1_score metric to make sure that the answer is correct and as you can see, 
the f1_score metric coincides with the one given by the model itself 
(you can see the calculation of the metric if you go to the page with the
model and open Metrics)
![alt text](images/show%20metrics.png)