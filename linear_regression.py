import mlflow
import numpy as np
from sklearn.linear_model import LinearRegression

mlflow.set_tracking_uri('http://127.0.0.1:5000')  # set up connection
mlflow.set_experiment('LinearRegression')  # set the experiment
mlflow.sklearn.autolog()


def make_data(num_points):
    x = np.random.rand(num_points, 1)
    e = np.random.rand(num_points, 1) / 10.0
    y = x + e
    return x, y


num_points = 1000
with mlflow.start_run() as run:
    x, y = make_data(num_points)

    model = LinearRegression()
    model.fit(x, y)

    mlflow.sklearn.log_model(
        sk_model=model,
        artifact_path="sklearn-model",
        registered_model_name="sk-learn-linear-regression-model",
    )
